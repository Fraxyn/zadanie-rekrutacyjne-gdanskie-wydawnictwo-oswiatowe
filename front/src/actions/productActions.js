import { FETCH_PRODUCTS } from '../types';


export const fetchProducts = () => async (dispatch) => {
    const res = await fetch('http://localhost:3001/api/book');
    const results = await res.json();
    dispatch({
        type: FETCH_PRODUCTS,
        payload: results.data,
    });
};
