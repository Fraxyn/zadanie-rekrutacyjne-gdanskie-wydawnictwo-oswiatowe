import React from 'react';
import Cart from './components/Cart';
import Products from './components/Products';
import store from './store';
import { Provider } from 'react-redux';

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <div className="grid-container">
                    <header>
                        <a href="/">KSIĘGARNIA</a>
                    </header>
                    <main>
                        <div className="content">
                            <div className="main">
                                <Products></Products>
                            </div>
                            <div className="sidebar">
                                <Cart></Cart>
                            </div>
                        </div>
                    </main>
                    <footer>Więcej informacji..</footer>
                </div>
            </Provider>
        );
    }
}

export default App;
