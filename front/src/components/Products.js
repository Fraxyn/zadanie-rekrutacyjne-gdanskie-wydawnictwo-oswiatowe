import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';
import { connect } from 'react-redux';
import { fetchProducts } from '../actions/productActions';
import { addToCart } from '../actions/cartActions';

class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: null,
        };
    }

    componentDidMount() {
        this.props.fetchProducts();
    }

    render() {
        return (
            <div>

                {!this.props.products ? (
                    <div>Loading...</div>
                ) : (
                    <ul className="products">
                        {this.props.products.map((product) => (
                            <li key={product.id}>
                                <div className="product">
                                    <a href={'#' + product.id}>
                                        <img src={product.cover_url} alt={product.title}></img>
                                    </a>
                                    <div className="description">
                                        <p>{product.title}</p>
                                        <p>{product.author}</p>
                                        <p>Stron w książce: {product.pages}</p>
                                    </div>

                                    <div className="product-price">
                                        <div>
                                            <div></div>
                                            <div>
                                                Cena: {product.price} {product.currency}
                                            </div>
                                        </div>
                                        <button onClick={() => this.props.addToCart(product)} className="button primary">
                                            Dodaj do koszyka
                                            </button>
                                    </div>
                                </div>
                            </li>
                        ))}
                    </ul>
                )}

            </div>
        );
    }
}

export default connect((state) => ({ products: state.products.items }), {
    fetchProducts,
    addToCart,
})(Products);
