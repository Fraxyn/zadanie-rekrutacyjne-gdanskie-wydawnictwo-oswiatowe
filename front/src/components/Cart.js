import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';
import { removeFromCart } from '../actions/cartActions';
import { connect } from 'react-redux';
import { createOrder, clearOrder } from '../actions/orderActions';
import Modal from 'react-modal';
import Zoom from 'react-reveal';

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: "",
            last_name: "",
            city: "",
            zip_code: "",
            showCheckout: false,

        };
    }
    handleInput = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    createOrder = (e) => {
        e.preventDefault();
        const { cartItems } = this.props;
        let orderItems = [];

        cartItems.forEach(function (item) {
            orderItems.push({
                id: item.id,
                quantity: item.quantity,
            });
        });

        const order = {
            order: orderItems,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            city: this.state.city,
            zip_code: this.state.zip_code,
        };

        this.props.createOrder(order);
    };

    componentWillMount() {
        Modal.setAppElement('#root');
    }

    closeModal = () => {
        this.props.clearOrder();
    };

    render() {
        const { cartItems, order } = this.props;

        return (
            <div>
                {cartItems.length === 0 ? (
                    <div className="cart cart-header">Koszyk jest pusty</div>
                ) : (
                    <div className="cart cart-header">Koszyk posiada:</div>
                )}


                {order && (
                    <Modal isOpen={true} onRequestClose={this.closeModal}>
                        <Zoom>
                            <button className="close-modal" onClick={this.closeModal}>x</button>
                            <div className="order-details">
                                <h3 className="success-message">Twoje zamówienie zostało złożone</h3>
                                <h2>zamówienie</h2>
                                <ul>
                                    <li>
                                        <div>Imię: </div>
                                        <div>{this.state.first_name}</div>
                                    </li>
                                    <li>
                                        <div>Nazwisko:</div>
                                        <div>{this.state.last_name}</div>
                                    </li>
                                    <li>
                                        <div>Miasto:</div>
                                        <div>{this.state.city}</div>
                                    </li>
                                    <li>
                                        <div>Kod pocztowy:</div>
                                        <div>{this.state.zip_code}</div>

                                    </li>
                                    <li>
                                        <div>Przedimoty w koszyku:</div>
                                        <div>
                                            {this.props.cartItems.map((x) => (
                                                <div key={x.id}>
                                                    {x.quantity}{" x "}{x.title}
                                                </div>
                                            ))}
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </Zoom>
                    </Modal>
                )}

                <div>
                    <div className="cart">
                        <Fade left cascade>
                            <ul className="cart-items">
                                {cartItems.map((item) => (
                                    <li key={item.id}>
                                        <div>
                                            <img src={item.cover_url} alt={item.title}></img>
                                        </div>
                                        <div>
                                            <div>{item.title}</div>
                                            <div className="right">
                                                {item.price} {item.currency} x Ilość: {item.quantity}{' '}
                                                <button className="button" onClick={() => this.props.removeFromCart(item)}>
                                                    Usuń z koszyka
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </Fade>
                    </div>
                    {cartItems.length !== 0 && (
                        <div>
                            <div className="cart">
                                <div className="total">
                                    <div>Suma: {cartItems.reduce((a, c) => a + c.price * c.quantity, 0)} PLN</div>
                                    <button
                                        onClick={() => {
                                            this.setState({ showCheckout: true });
                                        }}
                                        className="button primary"
                                    >
                                        DALEJ
                                    </button>
                                </div>
                            </div>
                            {this.state.showCheckout && (
                                <Fade right cascade>
                                    <div className="cart">
                                        <form onSubmit={this.createOrder}>
                                            <ul className="form-conteiner">
                                                <li>
                                                    <label>Imie</label>
                                                    <input name="first_name" type="text" required onChange={this.handleInput}></input>
                                                </li>
                                                <li>
                                                    <label>Nazwisko</label>
                                                    <input name="last_name" type="text" required onChange={this.handleInput}></input>
                                                </li>
                                                <li>
                                                    <label>Miejscowość</label>
                                                    <input name="city" type="text" required onChange={this.handleInput}></input>
                                                </li>
                                                <li>
                                                    <label>Kod pocztowy</label>
                                                    <input name="zip_code" type="text" required onChange={this.handleInput}></input>
                                                </li>
                                                <li>
                                                    <button className="button primary" type="submit">
                                                        Zamów
                                                    </button>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                </Fade>
                            )}
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        order: state.order.order,
        cartItems: state.cart.cartItems,
    }),
    {
        removeFromCart,
        createOrder,
        clearOrder,
    }
)(Cart);
